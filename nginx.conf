upstream ethnos_upstream {
    ip_hash;  # websocket connections need a constant server
    server localhost:5000;
}

server {
    server_name ${domain};
    listen 80;

    location / {
        # Redirect to use HTTPS
        return 301 https://$host$request_uri;
    }
}

server {
    server_name ${domain};
    listen 443 ssl;

    # Let's Encrypt certificate and key
    ssl_certificate /etc/ssl/certs/ethnos.pem;
    ssl_certificate_key /etc/ssl/private/ethnos-key.pem;

    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
    ssl_prefer_server_ciphers on;
    #ssl_dhparam /etc/nginx/dhparams.pem;

    root /var/www/chat;
    index index.html;

    location / {
        proxy_pass http://ethnos_upstream;
        proxy_http_version 1.1;
    }

    # Serve static files directly with nginx
    #location /static {
    #    alias /var/www/ethnos/static/;
    #    add_header Cache-Control "no-cache";
    #}

    location ~ ^/chat/(?<room_id>.+)/ws {
        # These settings are needed for websockets
        proxy_pass http://ethnos_upstream/chat/$room_id/ws;
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_read_timeout 300;
        proxy_send_timeout 300;
    }
}
